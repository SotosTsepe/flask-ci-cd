import pytest

from app import create_app
from config import TestConfig


@pytest.fixture
def client():
    app = create_app(TestConfig)
    assert app.config['TESTING'] is True
    return app.test_client()
