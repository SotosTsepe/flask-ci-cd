import os

from flask import request

from app import create_app


app = create_app()


@app.route('/reload', methods=('POST',))
def reload():
    if app.config['ADMIN_PASSWORD'] == request.form.get('admin_password'):
        try:
            os.system('git pull origin master')
            return {'message': 'Pulled successfully.', 'status': 200}
        except:
            return {'message': 'Pull failed.', 'status': 400}, 400

    return {'message': 'Authentication failed.', 'status': 400}, 400

