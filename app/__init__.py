from flask import Flask, redirect, url_for

from app.routes import register_routes
from config import Config


def create_app(config_class=Config):
    app = Flask(__name__)
    register_routes(app)
    app.config.from_object(config_class)

    @app.route('/')
    def index():
        return redirect(url_for('api.index'))

    return app
