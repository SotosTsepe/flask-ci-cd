from flask import Flask


def register_routes(app: Flask):
    from app.api import api_bp
    app.register_blueprint(api_bp)
