FROM debian:stable-slim

RUN apt-get update && apt-get install -y \
    git \
    python3 \
    python3-pip

COPY . /app
WORKDIR /app
RUN chmod +x scripts/deploy.sh

RUN pip3 install -r requirements.txt

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
